import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  personas: any[]= [];
  
  constructor( private http: HttpClient) {
    this.http
      .get("https://api-apross.herokuapp.com/api/personas")
      .subscribe((result: any[]) => {
        this.personas = result;
        console.log(result);
      });

   }

  ngOnInit() {
  }

}
