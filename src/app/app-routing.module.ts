import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactComponent } from './components/contact/contact.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'contact', component: ContactComponent},
  { path: '', pathMatch: 'full', redirectTo: 'dashboard'},
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
